import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import graph.GraphVisual;

import javax.swing.JFrame;

import production.Algorithm;
import production.BackwardChaining;
import production.ForwardChaining;
import production.Interpreter;
import production.RulesList;

public class Production {

	/**
	 * Aplikacijos pagrindinis metodas.
	 * 
	 * @param args Pirmu argumentu nurodome algoritmą (args[0]), o antru - kelią iki duomenų failo (args[1]).
	 * @throws UnsupportedEncodingException 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		RulesList rules;
		String algorithm, dataFile, buffer;
		Algorithm alg;
		PrintWriter writer;
		
		// Jeigu pateikti ne visi argumentai
		if (args.length < 2) {
			System.out.println("Naudojimo struktura: java -cp jgraphx.jar;. Production algoritmas (fc arba bc) data_file");
			System.out.println("Naudojimo pavyzdys: java -cp jgraphx.jar;. Production fc data.txt");
			return;
		}
		
		algorithm = args[0]; // Algoritmas
		dataFile = args[1]; // Duomenų failas
		switch(algorithm) {
			case "fc":
				alg = new ForwardChaining();
				break;
			case "bc":
				alg = new BackwardChaining();
				break;
			default:
				System.out.println("Algoritmas gali buti arba 'fc' (Forward Chaining), arba 'bc' (Backward Chaining).");
				return;
		}
		
		// Nuskaitome duomenų failą ir jį interpretuojame
		Interpreter.readFile(dataFile);
		
		// Sukuriame išvesties failą
		writer = new PrintWriter("output.txt", "UTF-8");
		
		// Surenkame taisyklių sąrašą
		rules = Interpreter.getRules();
		
		
		writer.println("1. Taisyklės, faktai, tikslas ir pasirinktas algoritmas\n");
		
		// Įrašome gautą produkcijų sistemą į išvesties failą
		writer.println(rules);
		
		// Indikuojame, kad bus vykdomas algoritmas
		writer.print("  1.4. Pasirinktas algoritmas: " + (algorithm.equals("fc") ? "Forward Chaining" : "Backward Chaining") + "\n");
		
		writer.println("\n2. Algoritmo vykdymas\n");
		
		// Vykdome pasirinktą algoritmą
		alg.execute(rules, writer);

		writer.println("3. Algoritmo vykdymo rezultatai\n");
		
		writer.println("  3.1. Tikslas pasiektas: " + (alg.isSolved() ? "Taip" : "Ne") + "\n");
		
		writer.print("  3.2. Gautas kelias: ");
		if (alg.isSolved()) {
			buffer = alg.getPath().toString().replace("[", "").replace("]", "");
			writer.print((buffer.equals("") ? "Tuščias" : buffer) + "\n\n");
		} else
			writer.print("Nėra\n\n");

		writer.println("  3.3. Semantinio grafo tekstinis išvedimas");
		writer.print(alg.getSemanticGraph(rules));
		writer.println("    Pastaba. Simbolis '-' žymi kintamojo viršūnę, o '_' - taisyklės. Be to, 'EX' (Exists) reiškia, kad tikslas yra faktų sąraše, o 'DNEX' (Does Not Exist) - nėra. 'EX ir 'DNEX' žymėjimai aktualūs tik Backward Chaining algoritmui.");
			
		System.out.println("Algoritmas darba baige. Tuojau bus atidarytas semantinio grafo vaizdas. Programa baigs darba, kai uzdarysite atidaryta langa.");
			
		// Pateikiame vykdyto algoritmo semantinio grafo gautą vaizdą
		GraphVisual frame = new GraphVisual(alg.getSemanticGraph(rules), "Semantinis grafas");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(700, 300);
		frame.setVisible(true);

		writer.close();
		
	}
	
}
