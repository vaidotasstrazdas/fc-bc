package production;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

public class ForwardChaining extends Algorithm {

	/**
	 * ForwardChaining algoritmo konstruktorius.
	 */
	public ForwardChaining() {
		super();
	}
	
	/**
	 * ForwardChaining algoritmo kvietimo ir vykdymo metodas.
	 * 
	 * Pastaba. Šio algoritmo vykdymo laikas yra polinominis dėl panaudotos skaičių priskyrimo lentelės, kuris
	 * leido taisyklių sąrašuose sudaryti faktų lenteles, kurios pasiekiamos per konstantinį vykdymo laiką.
	 * To nebūtų (t.y. algoritmas būtų įvykdomas per "log-polinominį" laiką), jeigu naudočiau kintamųjų pavadinimus, nes tada reikėtų
	 * ieškojimus atlikti objektuose, kurių tipas yra, pavyzdžiui, HashSet/HashMap, o paieška juose yra logaritminė.
	 * 
	 * @param rules Taisyklių sąrašas.
	 * @see RulesList
	 */
	public void execute(RulesList rules, PrintWriter writer) {
		boolean[] facts = rules.getBoolFacts(); // Paimame faktų lentelę
		HashMap<Integer, String> symbols = Rule.getIntegerKeys();
		ArrayList<String> missingFacts = new ArrayList<String>();
		int goal = rules.getIntGoal(), iteratorNo = 1, counter = 1; // Paimame tikslą
		
		boolean antecedentCheck, // Antecedentų buvimo faktų lentelėje tikrinimo kintamasis
		        statusChanges = true; // Būsenos tikrinimo kintamasis
		
		// Patikrinti, ar tikslas yra tarp faktų
		if (facts[goal]) {
			this.solved = true;
			writer.println("  Tikslas tarp faktų.\n");
		}
		
		// Kol būsena keičiasi ir problema nėra išspręsta
		while(statusChanges && !this.solved) {
			statusChanges = false; // Tariame, kad būsena nepasikeis
			writer.println("  Iteracija " + iteratorNo++);
			for(Rule rule: rules) {
				// Jeigu taisyklė pritaikyta arba jos konsekventas yra faktų lentelėje, ši taisyklė nebus apdorojama
				if (rule.isApplied() || facts[rule.getIntConsequent()]) {
					writer.print("  " + (counter++) + ") " + rule + " netaikome, nes ");
					 if (rule.isApplied())
							writer.print("ji jau pritaikyta.");
					 else if (facts[rule.getIntConsequent()])
						writer.print("jos konsekventas (" + rule.getConsequent() + ") yra faktas.");
					writer.println();
					continue;
				}
				
				// Antecedentų buvimo faktų lentelėje tikrinimo testas
				antecedentCheck = true;
				missingFacts.clear();
				for(int antecedent: rule.getIntAntecedents()) {
					if (!facts[antecedent]) {
						antecedentCheck = false;
						missingFacts.add(symbols.get(antecedent));
					}
				}
				
				// Jeigu antecedentų testas nepavyko, šią taisyklę praleidžiame
				if (!antecedentCheck) {
					writer.println("  " + (counter++) + ") " + rule + " praleidžiame, nes trūksta: " + missingFacts);
					continue;
				}
				
				/**
				 * Testas pavyko, todėl:
				 * 1. Nustatome, kad taisyklė pritaikyta.
				 * 2. Pridedame naują faktą į faktų lentelę.
				 * 3. Pridedame pritaikytą taisyklę į kelią.
				 */
				rule.setApplied();
				rules.addFact(rule.getConsequent());
				writer.println("  " + (counter++) + ") " + rule + " pritaikome. Dabar turimi faktai: " + rules.getFacts());
				
				facts[rule.getIntConsequent()] = true;
				
				this.path.add(rule.getName());
				
				
				statusChanges = true; // Akivaizdu, kad būsena pasikeitė
				
				/**
				 * Ar sprendinys rastas, priklauso nuo to, koks faktas pakliuvo į lentelę.
				 * Jeigu tai buvo goal, tuomet this.solved įgis true reikšmę ir ForwardChaining algoritmas kitos iteracijos nebevykdys,
				 * nes bus baigęs darbą.
				 */
				this.solved = facts[goal];
				writer.println();
				
				// Nutraukiame iteravimą per taisyklių sąrašą (nors, jeigu nebūtų break, gautume Negnevitsky algoritmą).
				break;
			}
			
			if (!statusChanges)
				writer.println();
		}
		
	}

}
