package production;

import java.io.PrintWriter;

public class BackwardChaining extends Algorithm {

	/**
	 * Taisyklių sąrašo faktų lentelė.
	 */
	private boolean[] facts;
	
	/**
	 * Taisyklių sąrašas (nereikės kiekvieną kartą rekursyviajame kvietinyje jo perduoti, nes jį turės einamasis objektas).
	 */
	private RulesList rules;
	
	/**
	 * Objektas, skirtas BackwardChaining algoritmo vykdymo išvedimui į failą.
	 */
	private PrintWriter writer;
	
	/**
	 * Pagrindinis tikslas
	 */
	private int finalGoal;
	
	/**
	 * Padės sekti, kurioje rekursijos dalyje esu
	 */
	private int nestingLevel;
	
	/**
	 * Padės nustatyti ciklus
	 */
	private boolean[] goals;
	
	/**
	 * Padės nustatyti, kada sugrįžta iš ciklo arba nepavyko išvesti taisyklės
	 */
	private boolean resolutionFail;
	
	/**
	 * Skaitliukas
	 */
	private int counter;
	
	/**
	 * BackwardChaining algoritmo konstruktorius.
	 */
	public BackwardChaining() {
		super();
		this.nestingLevel = 0;
	}

	/**
	 * BackwardChaining algoritmo kvietimo metodas, tačiau tai nėra vykdymo metodas.
	 * 
	 * @params rules Taisyklių sąrašas.
	 * @see RulesList.
	 */
	public void execute(RulesList rules, PrintWriter writer) {
		this.facts = rules.getBoolFacts();
		this.rules = rules;
		this.writer = writer;
		this.finalGoal = rules.getIntGoal();
		this.counter = 1;
		
		this.goals = new boolean[Rule.getSymbols().size() + 1];
		for (int i = 0; i < this.goals.length; i++)
			this.goals[i] = false;
		
		this.solved = this.doBC(this.finalGoal, null);
		
		this.writer.println();
	}

	/**
	 * BackwardChaining algoritmo vykdymo metodas.
	 * Rašydamas šį algoritmą, rėmiausi čia išdėstyta idėja:
	 * http://www3.cs.stonybrook.edu/~leortiz/teaching/6.034f/Fall05/rules/fwd_bck.pdf
	 * 
	 * @param goal Potikslis
	 * @param bcRule Taisyklė, iš kurios gautas potikslis.
	 * @return true, jeigu BC randa sprendinį, arba false, jeigu to padaryti nepavyksta
	 * @see Rule
	 */
	private boolean doBC(int goal, Rule bcRule) {
		boolean antecedentCheck; // Skirta patikrinti antecedentams
		String symbol = Rule.getIntegerKeys().get(goal); // Šito reikia tik semantiniam grafui ir esminių žingsnių reprezentavimui sugeneruoti.
		String symbolOld;
		
		this.nestingLevel++;
		this.writer.print("  " + (this.counter++) + ") " + repeat("  ", this.nestingLevel) + "Tikslas: " + symbol + ". ");

		if (this.facts[goal]) {
			// TAIP reiškia, kad produkcijų sistemos kintamasis priklauso faktų lentelei.
			this.writer.print("Faktas.");
			this.writer.println();
			this.nestingLevel--;
			symbol = "-EX: " + symbol;
			this.addGraphElement(symbol, bcRule, false);
			return true;
		} else {
			symbolOld = symbol;
			symbol = "-DNEX: " + symbol; // NE reiškia, kad produkcijų sistemos kintamasis nepriklauso faktų lentelei
		}
		
		if (this.goals[goal]) {
			this.writer.print("Ciklas.");
			this.writer.println();
			this.nestingLevel--;
			this.resolutionFail = true;
			return false;
		}
		
		this.goals[goal] = true;
		
		this.addGraphElement(symbol, bcRule, false);
		
		for(Rule rule: this.rules) {
			
			// Patikriname, ar taisyklė turi ieškomą potikslį
			if (rule.getIntConsequent() != goal)
				continue;
			
			this.addGraphElement(symbol, rule, true);
			if (this.resolutionFail) {
				this.writer.print("  " + (this.counter++) + ") " + repeat("  ", this.nestingLevel) + "Tikslas: " + symbolOld + ". ");
				this.resolutionFail = false;
			}
			this.writer.print("Imame " + rule + ". Tikslai: " + rule.getAntecedents() + ".");
			this.writer.println();
			
			// Tikriname, ar visi antecedentai išvedami kaip potiksliai
			antecedentCheck = true;
			for (int antecedent: rule.getIntAntecedents()) {
				if (!this.doBC(antecedent, rule)) {
					antecedentCheck = false;
					break;
				}
			}
			
			// Jeigu ne visi, tuomet imame kitą taisyklę
			if (!antecedentCheck)
				continue;
			
			this.writer.print("  " + (this.counter++) + ") " + repeat("  ", this.nestingLevel) + "Tikslas: " + symbolOld + ". Naujas faktas: " + symbolOld + ". ");
			this.rules.addFact(symbolOld);
			this.writer.print("Faktai: " + this.rules.getFacts());
			this.writer.println();
			this.nestingLevel--;
			
			// Kadangi antecedentai išvedami, tai įrašome naują taisyklę į kelią
			this.path.add(rule.getName());
			this.goals[goal] = false;
			
			return true;
		}
		
		String goalString = Rule.getIntegerKeys().get(goal);
		
		if (this.resolutionFail)
			this.writer.print("  " + (this.counter++) + ") " + repeat("  ", this.nestingLevel) + "Tikslas: " + symbolOld + ". ");
		this.writer.println("Tikslo " + goalString + " pasiekti nepavyko.");
		
		if (this.rules.getFacts().size() > 0 && this.path.size() > 0) {
			this.rules.getFacts().remove(this.rules.getFacts().size() - 1);
			this.path.remove(this.path.size() - 1);
		}
		
		this.resolutionFail = true;

		this.nestingLevel--;
		this.goals[goal] = false;
		// Bus false tuo atveju, jeigu nepavyks rasti produkcijų sistemos sprendimo einamajam potiksliui, naudojantis BackwardChaining algoritmu
		return false;
	}
	
	/**
	 * Metodas, skirtas sukurti reikiamą viršūnę semantiniame grafe, jeigu to reikia.
	 * Paprastai visada pridedamos naujos briaunos.
	 * 
	 * @param symbol
	 * @param rule
	 * @param inIteration
	 */
	private void addGraphElement(String symbol, Rule rule, boolean inIteration) {
		// Jeigu neiteruojame per taisykles, tuomet simboliui kuriame viršūnę
		if (!inIteration)
			this.graph.addVertex(symbol);
		
		if (rule != null) {
			// Sukuriame taisyklei viršūnę
			this.graph.addVertex("_" + rule.getName());
			
			/**
			 * Jeigu iteruojame per taisykles, tuomet kuriame tokią briauną, kad taisyklės viršūnė rodytų į simbolio viršūnę,
			 * t.y. rodo į kintamąjį, kuris yra taisyklės konsekventas.
			 * Jeigu neiteruojame per taisykles, tuomet simbolis rodo į taisyklę, iš kurios atėjo.
			 */
			if (inIteration)
				this.graph.addEdge("_" + rule.getName(), symbol);
			else
				this.graph.addEdge(symbol, "_" + rule.getName());
		}
	}
	
	private static String repeat(String what, int times) {
		return String.format(String.format("%%0%dd", times), 0).replace("0", what);
	}
	
}
