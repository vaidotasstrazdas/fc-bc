package production;

import java.util.ArrayList;
import java.util.HashMap;

public class Rule {

	/**
	 * Taisyklės pavadinimas.
	 * */
	private String name;
	
	/**
	 * Antecedentų sąrašas, išsaugotas skaičių formatu.
	 */
	private ArrayList<Integer> intAntecedents;
	
	/**
	 * Antecedentų sąrašas, išsaugotas pagal jų pavadinimus.
	 */
	private ArrayList<String> stringAntecedents;
	
	/**
	 * Kintamųjų pavadinimų lentelė su jiems priskirtais skaičiais.
	 */
	private static HashMap<String, Integer> integers;
	
	/**
	 * Kintamųjų pavadinimų lentelė su skaičiams priskirtais kintamųjų pavadinimais.
	 */
	private static HashMap<Integer, String> symbols;
	
	/**
	 * Konsekventas, išreikštas skaičiumi.
	 */
	private int intConsequent;
	
	/**
	 * Konsekventas, išreikštas simboliniu formatu.
	 */
	private String stringConsequent;
	
	/**
	 * Ar taisyklė buvo pritaikyta?
	 */
	private boolean applied = false;
	
	/**
	 * Konstruktorius Rule klasei.
	 * 
	 * @param name Taisyklės pavadinimas (R1, R2, Debesuotumas ir t.t.)
	 * @throws NullPointerException Tais atvejais, kai bandoma sukurti taisyklę null vardu.
	 * @throws IllegalArgumentException Tais atvejais, kai mėginama sukurti taisyklę tuščiu simboliu arba su krūva tarpų, tab'ų ir pan. be jokių kitų aiškių simbolių.
	 */
	public Rule(String name) {
		// Taisyklės pavadinimas negali būti null
		if (name == null)
			throw new NullPointerException();
		
		if (name.replaceAll("\\s", "").equals(""))
			throw new IllegalArgumentException();
		
		this.name = name;
		this.intAntecedents = new ArrayList<Integer>();
		this.stringAntecedents = new ArrayList<String>();
	}
	
	/**
	 * Pridėti antecedentą taisyklei.
	 * 
	 * @param antecedent Antecedento kintamasis.
	 */
	public void addAntecedent(String antecedent) {
		this.stringAntecedents.add(antecedent);
		this.intAntecedents.add(Rule.newSymbol(antecedent));
	}
	
	/**
	 * Nustatyti konsekventą.
	 * 
	 * @param consequent Konsekvento kintamasis.
	 */
	public void setConsequent(String consequent) {
		this.stringConsequent = consequent;
		this.intConsequent = Rule.newSymbol(consequent);
	}
	
	/**
	 * Ar taisyklė buvo pritaikyta?
	 * 
	 * @return true, jeigu taisyklė pritaikyta, ir false, jeigu nepritaikyta.
	 */
	public boolean isApplied() {
		return this.applied;
	}
	
	/**
	 * Nustatyti, kad taisyklė buvo pritaikyta.
	 */
	public void setApplied() {
		this.applied = true;
	}
	
	/**
	 * Gauti taisyklės pavadinimą.
	 * 
	 * @return Taisyklės pavadinimas String tipo.
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Gauti antecedentus, išreikštus skaičiais.
	 * 
	 * @return Antecedentų sąrašas, priskirtas taisyklės objektui.
	 */
	public ArrayList<Integer> getIntAntecedents() {
		return this.intAntecedents;
	}
	
	/**
	 * Gauti antecedentus, išreikštus jų kintamųjų pavadinimais.
	 * 
	 * @return Antecedentų sąrašas, išreikštas jų kintamųjų pavadinimais.
	 */
	public ArrayList<String> getAntecedents() {
		return this.stringAntecedents;
	}
	
	/**
	 * Gauti taisyklės konsekventą, išreikštą skaičiumi.
	 * 
	 * @return Konsekventas, išreikštas skaičiumi.
	 */
	public int getIntConsequent() {
		return this.intConsequent;
	}
	
	/**
	 * Gauti taisyklės konsekventą, išreikštą konsekvento kintamojo pavadinimu.
	 * 
	 * @return Konsekventas, išreikštas kintamojo pavadinimu.
	 */
	public String getConsequent() {
		return this.stringConsequent;
	}
	
	/**
	 * Šitaip bus galima išspausdinti taisyklę.
	 */
	public String toString() {
		String buffer = this.getName() + ": ", subBuffer;
		subBuffer = "";
		for(String antecedent: this.getAntecedents())
			subBuffer += antecedent + ", ";
		subBuffer = subBuffer.substring(0, subBuffer.length() - 2);
		buffer += subBuffer + " -> " + this.getConsequent();
		return buffer;
	}
	
	/**
	 * Gauti naudojamų kintamųjų produkcijų sistemoje lentelę,
	 * kurioje kiekvienam pavadinimui priskirtas koks nors skaičius.
	 * 
	 * @return Produkcijų sistemos kintamųjų lentelė.
	 */
	public static HashMap<String, Integer> getSymbols() {
		return Rule.integers;
	}
	
	/**
	 * Gauti naudojamų skaičių produkcijų sistemoje lentelę,
	 * kurioje kiekvienam skaičiui priskirtas tos sistemos kintamojo pavadinimas.
	 * 
	 * @return Produkcijų sistemos skaičių lentelė.
	 */
	public static HashMap<Integer, String> getIntegerKeys() {
		return Rule.symbols;
	}
	
	/**
	 * Pridėti naują simbolį į produkcijų sistemos lenteles.
	 * 
	 * @param symbol Simbolis (kintamasis), pridedamas į produkcijų sistemos lentelę.
	 * @return Indeksas, kuriuo simbolis (kintamasis) adresuojamas produkcijų sistemos kintamųjų lentelėje.
	 * @see RulesList
	 */
	public static int newSymbol(String symbol) {
		int index;
		
		// Sukuriame objektus tiek skaičių, tiek kintamųjų lentelei, jeigu bent viena lentelė nesukurta (bent viena -> abi nesukurtos)
		if (Rule.integers == null) {
			Rule.integers = new HashMap<String, Integer>();
			Rule.symbols = new HashMap<Integer, String>();
		}
		
		if (!Rule.integers.containsKey(symbol)) {
			// Sukuriame naują simbolį.
			index = Rule.integers.size();
			Rule.integers.put(symbol, index);
			Rule.symbols.put(index, symbol);
			
			// Apie naują simbolį "pranešame" visiems esantiems taisyklių sąrašo objektams, kad šie atsinaujintų savo faktų lenteles
			for(RulesList rlObject: RulesList.getObjects())
				rlObject.buildFactsTable();
			
		} else
			index = Rule.integers.get(symbol);
		
		return index;
	}
	
}
