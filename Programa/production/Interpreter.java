package production;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Interpreter {

	/**
	 * Taisyklių sąrašas, kuris bus sudarytas.
	 */
	private static RulesList rules;
	
	/**
	 * Ar faktai buvo rasti.
	 */
	private static boolean factsFound;
	
	/**
	 * Nuskaityti duoto duomenų failo eilutes ir tuomet jas interpretuoti į taisyklių sąrašą.
	 * Pastaba. Būtina nurodyti teisingą failo pavadinimą arba kelią iki failo.
	 * 
	 * @param filename Failo pavadinimas arba kelias iki jo.
	 */
	public static void readFile(String filename) {
		File file = new File(filename);
		ArrayList<String> lines = new ArrayList<String>();
		BufferedReader reader = null;
		String text;
		try {
			reader = new BufferedReader(new FileReader(file));
			while ((text = reader.readLine()) != null)
				lines.add(text);
		} catch (FileNotFoundException e) {
			System.out.println("Failas nerastas.");
		} catch (IOException e) {
			System.out.println("Klaida meginant nuskaityti faila.");
		} finally {
		    try {
		    	// Sudaryti taisyklių sąrašą
		    	Interpreter.interpret(lines);
		    	
		        if (reader != null)
		            reader.close();
		    } catch (IOException e) {
				System.out.println("Klaida uzdarant faila.");
		    }
		}

	}
	
	/**
	 * Sudarytas taisyklių sąrašas su visais faktais ir tikslu.
	 * 
	 * @return Taisyklių sąrašas.
	 * @see RulesList
	 */
	public static RulesList getRules() {
		return Interpreter.rules;
	}

	/**
	 * Nuskaitytų eilučių apdorojimo metodas.
	 * 
	 * @param lines Nuskaitytos eilutės.
	 */
	private static void interpret(ArrayList<String> lines) {
		int index = 0, length = lines.size() - 1;
		String line;
		Interpreter.rules = new RulesList();
		Interpreter.factsFound = false;
		while (index < length) {
			line = lines.get(index);
			
			// Jeigu eilutėje yra taisyklė, tai ją paimti
			Interpreter.getRule(line);
			
			// Jeigu faktai dar nesurinkti, tai juos surinkti
			if (!Interpreter.factsFound)
				Interpreter.getFacts(line, lines.get(index + 1));
			
			index++;
		}
		
		// Paimti tikslą (jis visada bus paskutinėje failo eilutėje)
		Interpreter.rules.setGoal(lines.get(index));
		
	}

	/**
	 * Jeigu eilutėje yra taisyklė, tai ją paimti ir išsaugoti taisyklių sąraše.
	 * 
	 * @param line Failo eilutė.
	 */
	private static void getRule(String line) {
		String ruleRegex = "([^:/]+):([^->]+)->([^->]+)", // Reguliari išraiška, skirta nustatyti, ar tai yra taisyklė.
			   ruleName, consequent;
		String[] antecedents;
		Rule rule;
		Pattern rulePattern = Pattern.compile(ruleRegex);
		Matcher match = rulePattern.matcher(line);
		
		// Tikriname, ar sukompiliuota reguliari išraiška mums duoda taisyklę.
		if (!match.find())
			return;
		
		// Iš reguliarios išraiškos rezultatų paimame taisyklės pavadinimą, antecedentus ir konsekventą
		ruleName = match.group(1);
		antecedents = match.group(2).replaceAll("\\s", "").split(",");
		consequent = match.group(3).replaceAll("\\s", "");
		
		// Sukuriame taisyklę ir įkeliame antecedentus bei galiausiai nurodome konsekventą
		rule = new Rule(ruleName);
		for (int i = 0; i < antecedents.length; i++)
			rule.addAntecedent(antecedents[i]);
		rule.setConsequent(consequent);
		
		// Sukurtą taisyklę išsaugome taisyklių sąraše
		Interpreter.rules.add(rule);
		
	}

	/**
	 * Surinkti faktus, jeigu jie yra duotoje failo eilutėje.
	 * 
	 * @param line Nuskaityta eilutė.
	 * @param emptyLine Eilutė po nuskaitytos eilutės, kuri turi būti tuščia pagal duotą duomenų formatą.
	 */
	private static void getFacts(String line, String emptyLine) {
		String factRegex = "^(?!\\s)([^:/->]+)$", // Reguliari išraiška faktui
			   matchedFacts;
		String[] facts;
		int i;
		
		// Ar kita nuskaityta eilutė išties tuščia?
		if (!emptyLine.equals(""))
			return;
		
		Pattern factPattern = Pattern.compile(factRegex);
		Matcher match = factPattern.matcher(line);
		
		// Sukompiliavę fakto eilutę, patikriname, ar ji atitinka reguliarią išraišką faktui
		if (!match.find())
			return;
		
		/**
		 *  Jeigu atitinka, tuomet mes surenkame GALIMUS faktus.
		 *  Faktai gali būti atskirti kableliais arba VIENU tarpu pagal mano duomenų formatą.
		 */
		matchedFacts = match.group(0);
		facts = matchedFacts.contains(",") ? matchedFacts.replaceAll("\\s", "").split(",") : matchedFacts.split(" ");
		
		// Faktai išties yra faktai, todėl juos išsaugome taisyklių sąraše
		for (i = 0; i < facts.length; i++)
			Interpreter.rules.addFact(facts[i]);

		// Pažymime, kad interpretatorius faktus surado
		Interpreter.factsFound = true;
	}
	
}
