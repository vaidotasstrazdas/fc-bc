package production;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class RulesList implements Iterable<Rule> {

	/**
	 * Visų RulesList objektų sąrašas (padės atnaujinti faktų lenteles, jeigu to reikės).
	 */
	private static ArrayList<RulesList> objects;
	
	/**
	 * Tikslas, išreikštas skaičiumi.
	 */
	private int intGoal;
	
	/**
	 * Tikslas, išreikštas simboliais.
	 */
	private String stringGoal;
	
	/**
	 * Taisyklių sąrašo faktų lentelė.
	 */
	private boolean[] boolFacts;
	
	/**
	 * Taisyklių sąrašo faktų sąrašas, kuriame esantys faktai yra išreikšti String tipo duomenimis.
	 */
	private ArrayList<String> stringFacts;
	
	/**
	 * Taisyklių sąrašas.
	 */
	private ArrayList<Rule> rules;
	
	/**
	 * Taisyklių lentelė pagal kiekvienos taisyklės pavadinimą.
	 * Šitaip bus lengviau gauti konkrečią taisyklę pagal jos pavadinimą (to reikia semantiniam grafui generuoti).
	 */
	private HashMap<String, Rule> rulesTable;
	
	/**
	 * RulesList konstruktorius.
	 */
	public RulesList() {
		
		// Jeigu RulesList.objects dar nesukurtas, tai sukuriame
		if (RulesList.objects == null)
			RulesList.objects = new ArrayList<RulesList>();
		RulesList.objects.add(this); // Pridedame naują RulesList objektą į sąrašą
		
		this.stringFacts = new ArrayList<String>();
		this.rules = new ArrayList<Rule>();
		this.rulesTable = new HashMap<String, Rule>();
	}
	
	/**
	 * Gauti RulesList objektus.
	 * 
	 * @return Sukurtų RulesList objektų sąrašas.
	 */
	public static ArrayList<RulesList> getObjects() {
		return RulesList.objects;
	}
	
	/**
	 * Sukurti faktų lentelę, remiantis egzistuojančia simbolių lentele.
	 */
	public void buildFactsTable() {
		boolean[] oldTable = this.boolFacts != null ? this.boolFacts : null;
		int length = Rule.getSymbols().size();
		this.boolFacts = new boolean[length + 1];
		for (int i = 0; i <= length; i++)
			this.boolFacts[i] = oldTable != null && i < oldTable.length ? oldTable[i] : false;
	}
	
	/**
	 * Įdėti naują taisyklę į taisyklių sąrašą.
	 * 
	 * @param rule Taisyklė
	 * @see Rule
	 */
	public void add(Rule rule) {
		// Taisyklė negali būti null
		if (rule == null)
			throw new NullPointerException();
		
		// Taisyklių pavadinimai turi būti skirtingi (to reikia semantiniam grafui)
		if (this.rulesTable.containsKey(rule.getName()))
			throw new IllegalArgumentException("Duplicate Key.");
		
		// Įdėti naują taisyklę
		this.rules.add(rule);
		
		// Taip pat įdėti tą pačią taisyklę į taisyklių lentelę greitesnei paieškai
		this.rulesTable.put(rule.getName(), rule);
		
	}
	
	/**
	 * Gauti taisyklių sąrašą.
	 * 
	 * @return Taisyklių sąrašas.
	 * @see Rule
	 */
	public ArrayList<Rule> getRules() {
		return this.rules;
	}
	
	/**
	 * Gauti taisyklę pagal jos pavadinimą.
	 * 
	 * @param name Taisyklės pavadinimas.
	 * @throws NoSuchElementException Jeigu taisyklės taisyklių sąraše pagal taisyklės pavadinimą nėra.
	 * @return Taisyklė pagal nurodytą pavadinimą.
	 */
	public Rule getRule(String name) {
		if (!this.rulesTable.containsKey(name))
			throw new NoSuchElementException();
		
		return this.rulesTable.get(name);
	}
	
	/**
	 * Pridėti faktą į taisyklių sąrašą.
	 * 
	 * @param fact Faktas.
	 * @throws NoSuchElementException Jeigu nurodytas simbolis neegzistuoja esamoje simbolių lentelėje.
	 */
	public void addFact(String fact) {
		if (!Rule.getSymbols().containsKey(fact))
			Rule.newSymbol(fact);
		
		this.boolFacts[Rule.getSymbols().get(fact)] = true;
		this.stringFacts.add(fact);
	}
	
	/**
	 * Nustatyti tikslą taisyklių sąrašui.
	 * 
	 * @param goal Tikslas.
	 * @throws NoSuchElementException Jeigu nurodytas simbolis neegzistuoja esamoje simbolių lentelėje.
	 */
	public void setGoal(String goal) {
		if (!Rule.getSymbols().containsKey(goal))
			Rule.newSymbol(goal);
		
		this.intGoal = Rule.getSymbols().get(goal);
		this.stringGoal = goal;
	}
	
	/**
	 * Gauti faktus, išreikštus jų simboliniais pavadinimais.
	 * 
	 * @return Faktai, išreikšti simboliniu pavadinimu.
	 */
	public ArrayList<String> getFacts() {
		return this.stringFacts;
	}
	
	/**
	 * Gauti faktų lentelę, adresuojamą pagal simbolių numerius.
	 * 
	 * @return Faktų lentelė.
	 */
	public boolean[] getBoolFacts() {
		return this.boolFacts;
	}
	
	/**
	 * Gauti tikslą, išreikštą simboliniu formatu.
	 * 
	 * @return Tikslas, išreikštas simboliniu formatu.
	 */
	public String getGoal() {
		return this.stringGoal;
	}
	
	/**
	 * Gauti tikslą, išreikštą skaičiaus formatu.
	 * 
	 * @return Tikslas, išreikštas skaičiaus formatu.
	 */
	public int getIntGoal() {
		return this.intGoal;
	}
	
	/**
	 * Taisyklių sąrašo iteratorius.
	 * @see Rule
	 */
	public Iterator<Rule> iterator() {
		return new RulesIterator();
	}
	
	/**
	 * Metodas, skirtas grąžinti taisyklių sąrašo bendrą vaizdą simboliniu formatu (taisyklės, faktai ir tikslas).
	 */
	public String toString() {
		String buffer = "";
		buffer += "  1.1. Taisyklių sąrašas\n";
		for(Rule rule: this)
			buffer += "    " + rule.toString() + "\n";
		
		buffer += "\n";
		buffer += "  1.2. Faktai: " + this.stringFacts + "\n\n";
		buffer += "  1.3. Tikslas: " + this.getGoal() + "\n";
		
		return buffer;
	}
	
	/**
	 * Taisyklių sąrašo iteratoriaus klasė.
	 * 
	 * @author Vaidotas Strazdas
	 *
	 */
	private class RulesIterator implements Iterator<Rule> {

		private int index = 0;
		
		public boolean hasNext() {
			return this.index < rules.size();
		}

		public Rule next() {
			return rules.get(this.index++);
		}

		public void remove() {
			throw new java.lang.UnsupportedOperationException();
		}
		
	}

}
