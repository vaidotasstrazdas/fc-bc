package production;

import java.io.PrintWriter;
import java.util.ArrayList;

import graph.Graph;


public abstract class Algorithm {

	/**
	 * Ar produkcijų sistemos problema išspręsta?
	 */
	protected boolean solved = false;
	
	/**
	 * Produkcijų sistemos sprendimo kelias.
	 */
	protected ArrayList<String> path;
	
	/**
	 * Produkcijų sistemos sprendimo grafas.
	 */
	protected Graph graph;
	
	/**
	 * Konstruktorius algoritmui (iš esmės jį panaudos kiti algoritmai).
	 * Pastaba. Visada būtina kviesti super(), rašant kokį nors algoritmą, prieš ką nors darant
	 * konstruktoriuje. Net jeigu nieko nedaroma, konstruktorių su super() vis tiek reikia sukurti,
	 * nes taip sukuriamos būtinos struktūros - kelio ir grafo objektai.
	 */
	public Algorithm() {
		this.path = new ArrayList<String>();
		this.graph = new Graph();
	}
	
	/**
	 * Konkretais algoritmo (pvz. ForwardChaining ir BackwardChaining) vykdymo metodas.
	 * 
	 * @param rules Taisyklių sąrašas.
	 * @see RulesList
	 */
	public abstract void execute(RulesList rules, PrintWriter writer);
	
	/**
	 * Išvalyti algoritmą (bus galima naudoti iš naujo, pavyzdžiui, su kitu sąrašu, nenaikinant jau esamų objektų).
	 */
	public void flush() {
		this.solved = false;
		this.path = new ArrayList<String>();
		this.graph = new Graph();
	}
	
	/**
	 * Produkcijų sistemos kelias.
	 * 
	 * @return Produkcijų sistemos sprendimo kelias.
	 */
	public ArrayList<String> getPath() {
		return this.path;
	}
	
	/**
	 * Gauti produkcijų sistemos sprendimo semantinį grafą.
	 * ForwardChaining algoritmui užtenka tiesiog pagal jau esamą kelią sugeneruoti šį grafą,
	 * tačiau BackwardChaining šį metodą tektų arba perrašyti, arba kaip nors įterpti į algoritmą.
	 * 
	 * @param rules Taisyklių sąrašas
	 * @return Produkcijų sistemos sprendimo semantinis grafas.
	 * @see Graph
	 * @see RulesList
	 */
	public Graph getSemanticGraph(RulesList rules) {
		return this.generateGraph(rules);
	}
	
	/**
	 * Ar produkcijų sistema išspręsta?
	 * 
	 * @return true, jeigu sistema išspręsta, ir false, jeigu neišspręsta.
	 */
	public boolean isSolved() {
		return this.solved;
	}
	
	/**
	 * Sugeneruoti produkcijų sistemos sprendimo kelio grafą (ForwardChaining algoritmui tai kartu bus ir semantinis grafas).
	 * Pastaba. Tikrieji viršūnių pavadinimai prasideda simboliu '_' arba '-'. Simbolis '_' reiškia, kad tai yra taisyklės viršūnė,
	 * o simbolis '-' - kad produkcijų sistemos kintamojo.
	 * 
	 * @param rules Taisyklių sąrašas.
	 * @return Sprendimo kelio grafas.
	 * @see Graph
	 * @see RulesList
	 */
	private Graph generateGraph(RulesList rules) {
		// Jeigu yra bent viena viršūnė, reiškia, generavimas jau kažkuriuo momentu vyko,
		// arba grafas buvo sugeneruotas kokio nors algoritmo vykdymo metu
		Rule rule;
		String ruleVertex, antecedentVertex;
		
		if (this.graph.V() > 0)
			return this.graph;
		
		for(String ruleName: this.path) {
			rule = rules.getRule(ruleName);
			ruleVertex = "_" + ruleName;
			this.graph.addVertex(ruleVertex);
			for(String antecedent: rule.getAntecedents()) {
				antecedentVertex = "-" + antecedent;
				if (!this.graph.hasVertex(antecedentVertex))
					this.graph.addVertex(antecedentVertex);
				this.graph.addEdge(antecedentVertex, ruleVertex);
			}
			this.graph.addEdge(ruleVertex, "-" + rule.getConsequent());
		}
		
		return this.graph;
		
	}
	
}
