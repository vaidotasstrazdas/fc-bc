package graph;

import java.util.HashMap;
import javax.swing.JFrame;
import javax.swing.SwingConstants;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.util.mxMorphing;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource.mxIEventListener;
import com.mxgraph.view.mxGraph;

public class GraphVisual extends JFrame {

	private static final long serialVersionUID = 1L;

	/**
	 * GraphVisual konstruktorius.
	 * 
	 * @param graph Grafas
	 * @param title Lango pavadinimas
	 * @see Graph
	 */
	public GraphVisual(Graph graph, String title) {
		super(title);
		final mxGraph visualGraph = new mxGraph();
		Object parent = visualGraph.getDefaultParent();
		HashMap<String, Object> vertexMap = new HashMap<String, Object>();
		String params;

		visualGraph.getModel().beginUpdate();
        try {
        	for (String vertex: graph.vertices()) {
        		params = vertex.startsWith("-") ? "shape=rectangle;perimeter=rectanglePerimeter" : "shape=ellipse;perimeter=ellipsePerimeter";
        		vertexMap.put(vertex, visualGraph.insertVertex(parent, null, vertex.substring(1), 0, 0, 40, 30, params));
        	}
            for (String vertex: graph.vertices()) {
                for (String v: graph.adjacentTo(vertex))
                	visualGraph.insertEdge(parent, null, "", vertexMap.get(vertex), vertexMap.get(v));
            }

        } finally {
        	visualGraph.getModel().endUpdate();
        }

		mxGraphComponent graphComponent = new mxGraphComponent(visualGraph);
		getContentPane().add(graphComponent);
		
        // Apibrėžti maketavimo schemą
		mxHierarchicalLayout layout = new mxHierarchicalLayout(visualGraph);
        layout.setOrientation(SwingConstants.WEST);
        
        // Vykdyti maketavimą
        visualGraph.getModel().beginUpdate();
        try {
            layout.execute(parent);
        } finally {
            mxMorphing morph = new mxMorphing(graphComponent, 20, 1.2, 20);

            morph.addListener(mxEvent.DONE, new mxIEventListener() {

                public void invoke(Object arg0, mxEventObject arg1) {
                	visualGraph.getModel().endUpdate();
                }

            });

            morph.startAnimation();
        }
	}
	
}
